#!/usr/bin/env python
# -*- coding:utf-8 -*-

""" Wordpress Deploy configuration """
import os

# Add ssh-reachable hosts here
HOSTS = ['user@ip.server']

PASS = ''
# percorso remoto sul server dove verra installato wp
CODE_DIR_REMOTE="/var/www/html/"

# percorso locale dove verra installato wp
CODE_DIR_LOCAL="/var/www/html/" #es. '%s/dev/wordpress/' % os.getenv('HOME')  es. /var/www/html/nomeprogetto


#il nome del tema da attivare 
THEME_GIT_NAME = ""

# What your git remote location is called
# You'll have to add these to both your local and remote branch!
GIT_REMOTE_NAME = 'origin'

# il nome del repo da dove prendere il progetto wp es. git@bitbucket.org:iangi/NOMEREPO.git
REPO_URL = ""

# Fill out these database details
# Currently, there's only two database hosts supported,
# local dev and remote prod (or staging)
DBASE = {
	'local':{ # qui i dati del database locale
		'host':'localhost',
		'user':'root',
		'password':'',
		'name':'',
	},
	'remote':{#inserire qui i dati del db per il server  
		'host':'localhost',
		'user':'root',
		'password':'',
		'name':'',
	},
	'hosting' : {
		'host': '',
		'user' : '',
		'password': '',
		'name' : '',

	},
}

#WORDPRESS

# nome del sito wp
SITE_NAME = ""

# dati per l installazione di wp, in locale e in remoto
WPCONFIG ={
	'local':{ # locale 
		'url':'',#
		'admin_user':'',#http://localhost/nomesito/
		'admin_pass':'',
		'admin_email':'',
	},
	'remote':{ # server 
		'url':'',#http:/ip.server/nomeprogetto
		'admin_user':'',
		'admin_pass':'',
		'admin_email':'',
	},
	'hosting':{ # server 
		'url':'',#http:/www.nomesito.it
		'admin_user':'',
		'admin_pass':'',
		'admin_email':'',
	},

}

WP_MAIN_MENU_NAME = "menu-1"

AREA_RISERVATA_SLUG ="area_riservata"

EXTRA_SECURE_WPCONFIG = '''<<PHP 
define( 'FS_CHMOD_FILE',0644);
define( 'FS_CHMOD_DIR',0755);
define( 'DISALLOW_FILE_EDIT', true);
define( 'WP_ADMIN_DIR', '%s');
define( 'ADMIN_COOKIE_PATH', SITECOOKIEPATH . WP_ADMIN_DIR);
PHP''' % AREA_RISERVATA_SLUG

EXTRA_HTACCESS_SEC = '''
RewriteEngine On
RewriteRule ^%s/(.*) wp-admin/$1?%s [L]
# disable directory browsing
Options All -Indexes
<files wp-config.php>
order allow,deny
deny from all
</files>
<Files licenza.html>
order allow,deny
deny from all
</Files>
<Files license.txt>
order allow,deny
deny from all
</Files>
<Files wp-config.php>
order allow,deny
deny from all
</Files>
<Files .htaccess>
order allow,deny
deny from all
</Files>

''' % (AREA_RISERVATA_SLUG, "%{QUERY_STRING}")