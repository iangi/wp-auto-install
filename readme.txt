﻿Requisiti:
* wp-cli :  wp-cli (  seguire la procedura per linux )
* fabric: “ sudo apt-get install fabric ”
* comopser: “ curl -s https://getcomposer.org/installer | php “ “ sudo mv composer.phar /usr/local/bin/composer ” ( necessita curl )
* git: “ sudo apt-get install git  “ 
* ssh: guida ( la chiave pubblica, per modificare il codice del repository,  deve essere aggiunta al progetto dall admin ) 


Gli ambienti di sviluppo devono aver installato questi software, sia in locale che sul server.




Procedura:
installazione locale: 
la procedura per installare un nuovo progetto wp in locale necessita di:
* creare su repository online (es. bitbuket) un nuovo progetto.
* creare in locale un db per il progetto.
* creare in locale  una cartella per il progetto ( es. /var/www/html/nomeprogetto ).
* Installare tutti i software necessari elencati nella sezione ‘requisiti’. 
* scaricare nella cartella locale del progetto  i files base per la procedura di auto install di wp (git clone git@bitbucket.org:iangi/wp-auto-install.git). 
* copiare il file config-dist.py e rinominarlo config.py. 
* Editare il contenuto di confing.py inserendo nelle varie variabili le specifiche del progetto:
   * CODE_DIR_LOCAL: il percorso della cartella creata (es.  “/var/www/html/nomeprogetto”)
   * SITE_NAME: il nome del sito
   * THEME_GIT_NAME: il nome del tema che verrà installato e attivato insieme a wordpress ( es. “onetone” o “onetone-child” se si usa un tema modificato )
   * DBASE: nella sezione local>host: nome dell host del db locale (es. “localhost” ), local>user: nome dell utente mysql locale (es. “root” ), local>password: password mysql locale, local>name: nome del db creato in precedenza
   * WPCONFIG: local>url: url del nuovo progetto wp locale (es. “http://localhost/momeprogetto”), local>admin_user: username dell utente admin di wordpress  locale (es. admin ), admin_pass:  password dell utente admin locale, admin_email: email  utente admin di wp in locale.
* Editare il file composer.json inserendo i plugins e il theme che verranno installati insieme a wp( i vari pacchetti, formattati per composer, si possono trovare qui: http://wpackagist.org/.)
* editare il file fix-wordpress-permissions_local.sh inserendo user locale, directory del progetto locale, eventualmente gruppo www
* Una volta configurato tutto accedere tramite terminale alla  cartella del progetto digitare “ fab install_wp “ per installare wordpress in locale ( si consiglia di fare anche un composer self update se richiesto ).
* Editare il file .gitignore ( nascosto ) per includere nel repo plugins o themes custom(es. !wp-content/themes/onetone-child ).
* Aggiungere i nuovi file al repo: “ git add . “, committare: “ git commit -am’commit iniziale’ ”, eseguire un push “ git push origin master “.



Adesso è possibile accedere al sito di wordpress in locale all indirizzo inserito nella variabile WPCONFIG>local>url. per accedere al pannello: urllocale/nomedelprogeto/wp-admin poi con i dati inseriti nelle varibili WPCONFIG>local>admin_user e admin_pass sarà possibile accedere.


installazione remota: 
Dopo aver installato wp in locale e aver fatto il commit e il push,
aggiungere la chiave del server nel pannello di admin del repo online (come: https://bitbucket.org/iangi/bluedrone/admin/deploy-keys/) .
 Per installare wp in remoto basta configurare il file config.py inserendo i dati relativi al server remoto:
* HOST: utente@server (es. user_server@86.78.9.. ),
* PASS: password utente server,
* CODE_DIR_REMOTE: inserire il percorso sul server dove si desidera installare wp, se non esiste sul server verrà creato in automatico, ( es. /var/www/html/nomeprogetto )
* REPO_URL: la url per poter clonare il progetto ( es. “git@bitbucket.org:iangi/nomeprogetto.git” )
* i dati del database del server nel dizionario DBASE nella sezione remote
* i dati relativi a wordpress che varrà installato sul  server in  WPCONFIG sempre nella sezione remote.
* editare il file fix-wordpress-permissions_server.sh inserendo user del server, directory del progetto sul server, eventualmente gruppo www
* digitando dalla cartella locale del progetto “ fab deploy_start ”: verrà creata sul server la cartella inserita in precedenza nella variabile CODE_DIR_REMOTE del file config.py, nella quale verrà installato wp, in automatico verrà creato il database sul server con il nome inserito nella variabile DBASE ->remote->name e creato l’ utente  admin con i  parametri di configurazione inseriti nella sezione remote della variabile WPCONFIG. Si potrà quindi accedere alla sezione di admin di wordpress con i dati inseriti “admin_user” “admin_pssword” all indirizzo del server (http://percorsoserver/wp-admin,  verranno inoltre installati i plugins e il thema inseriti nel file composer.json.


[* DA COMPLETARE DOCUMENTAZIONE SEZIONE AGGIORNAMENTI *] 

guide:
https://deliciousbrains.com/storing-wordpress-in-git/
https://github.com/fheinle/wordpress-fabfile
http://www.onextrapixel.com/2010/01/30/13-useful-wordpress-sql-queries-you-wish-you-knew-earlier/
http://wp-cli.org/commands/