# -*- coding: utf-8 -*-
#! /usr/bin/env python
import os
import glob
from urllib2 import urlopen
import json
import time
import lxml.etree as ET
from xml.etree import ElementTree as et
import git

from fabric.api import *
from fabric.operations import put, sudo
from fabric.contrib.files import exists
from fabric.contrib.project import rsync_project
from StringIO import StringIO
import paramiko
from config import HOSTS, CODE_DIR_LOCAL, PASS, \
                   CODE_DIR_REMOTE, DBASE, GIT_REMOTE_NAME, WPCONFIG, \
                   SITE_NAME, THEME_GIT_NAME, REPO_URL, WP_MAIN_MENU_NAME,\
                    EXTRA_SECURE_WPCONFIG, EXTRA_HTACCESS_SEC
env.hosts = HOSTS

env.password = PASS


def install_wp(location="local"):
    if location == "local":
        install_wp_core_local()
        istall_wp_extra_local()
        local(_make_active_theme_string())
        local("wp plugin activate wordpress-importer")
        local(_active_main_menu())
        adjust_permitions("local")
    else:
        _get_latest_source(CODE_DIR_REMOTE)
        install_wp_core_remote()
        istall_wp_extra_remote()
        run(_make_active_theme_string())
        run("wp plugin activate wordpress-importer")
        run(_active_main_menu())

def update_wp(location="local"):
    if location == "local":
        update_wp_core_local()
        update_wp_extra_local()
        local("wp site empty --yes")
        local("wp plugin install wordpress-importer --activate")
        update_wp_content_local()
        local(_make_active_theme_string())
        update_wp_theme_option_local()
        local(_active_main_menu())
    elif location == "hosting":
        update_wp_core_hosting()
        update_wp_extra_hosting()
        local("wp site empty --yes")
        local("wp plugin install wordpress-importer --activate")
        update_wp_content_local()
        local(_make_active_theme_string())
        update_wp_theme_option_local()
        local(_active_main_menu())

    else:
        _get_latest_source(CODE_DIR_REMOTE)
        update_wp_core_remote()
        update_wp_extra_remote()
        with cd(CODE_DIR_REMOTE):
            run("wp site empty --yes")
            run("rm wp-config.php")
            run(_make_config_wp_string(location="remote"))
            run("wp plugin install wordpress-importer --activate")
            run("wp db import export/db_options.sql")
            #run("wp plugin activate wordpress-importer")
            run(_make_active_theme_string())
            #
            run(_make_update_wp_content_string(CODE_DIR_REMOTE))
            with settings(warn_only=True):
                update_wp_theme_option_remote()
                run(_active_main_menu())
            clean_remote_update()


def local_import_last(wordpressDir):
    with lcd(wordpressDir):
        local("wp site empty --yes")
        local("wp plugin install wordpress-importer --activate")
        local("wp db import %s/export/server/db_options_posts.sql" % CODE_DIR_LOCAL)
        local("wp plugin activate wordpress-importer")
        local(_make_active_theme_string())
        newest = max(glob.iglob('%s/export/server/*.[Xx][Mm][Ll]'  % CODE_DIR_LOCAL), key=os.path.getctime)
        news_name = os.path.basename(newest)
        local("wp import %s/export/server/%s --authors=skip" % (CODE_DIR_LOCAL, news_name) )
        
        
        url = '%s/export/server/thememod.json' % WPCONFIG['local']['url']
        updates = _make_update_wp_theme_option_list(url = url)
        with settings(warn_only=True):
            make_wpcli_theme_updates(updates, location="local", invert=True)
        
def import_from_server(dobk=True):
    if dobk:
        back_up('local')
        export_wp_content(location="remote")
    remote_uploads_dir ="%s/wp-content/uploads" % CODE_DIR_REMOTE
    local_uploads_dir ="%s/wp-content/" % CODE_DIR_LOCAL
    remote_export_dir = "%s/export/server/" % CODE_DIR_REMOTE
    local_export_dir = "%s/export/" % CODE_DIR_LOCAL
    
    local("sudo rsync -rtvuc -status --ignore-existing %s:%s %s" % (HOSTS[0],remote_uploads_dir,  local_uploads_dir) )
    # rsync -avz -e ssh ufficiosrv@83.149.148.174:/var/www/html/siti/oltre-la-cantina/wp-content/uploads  /var/www/html/blueoak/oltre-la-cantina/wp-content
    #rsync -e ssh --ignore-existing file.to.upload user@server1.cyberciti.biz:/path/to/dest/
    #rsync -avz -e ssh user@remote-ip-address:/download/from/this/path /download/to/this/pat
    get(remote_path=remote_export_dir, local_path=local_export_dir)
    with lcd(CODE_DIR_LOCAL):
        local("wp site empty --yes")
        local("wp plugin activate wordpress-importer")
        local("wp db import export/server/db_options_posts.sql")
        local("wp plugin activate wordpress-importer")
        local(_make_active_theme_string())
        newest = max(glob.iglob('%s/export/server/*.[Xx][Mm][Ll]'  % CODE_DIR_LOCAL), key=os.path.getctime)
        news_name = os.path.basename(newest)
        local("wp import %s/export/server/%s --authors=skip" % (CODE_DIR_LOCAL, news_name) )
        
        
        url = '%s/export/server/thememod.json' % WPCONFIG['local']['url']
        updates = _make_update_wp_theme_option_list(url = url)
        with settings(warn_only=True):
            make_wpcli_theme_updates(updates, location="local", invert=True)
        clean_local_import()
    print " Import Completato"
        
    #return "wp import %s/export/%s --authors=skip" % (code_dir, news_name)

def back_up(location):
    timestr = time.strftime("%Y%m%d-%H%M%S")
    if location == "local":
        with lcd(CODE_DIR_LOCAL):
            local("wp db export --add-drop-table export/bk/db_%s.sql" % timestr)
        with lcd("%s/export/bk/" % CODE_DIR_LOCAL):
            local("tar cvf content_%s.tar %s --exclude 'export/bk/*'" % (timestr, CODE_DIR_LOCAL) )
    else:
        with cd(CODE_DIR_REMOTE):
            run("wp db export --add-drop-table export/server/bk/db_%s.sql" % timestr)
        with lcd("%s/export/server/bk/" % CODE_DIR_REMOTE):
            run("tar cvf content_%s.tar %s --exclude 'export/server/bk/*'" % (timestr, CODE_DIR_REMOTE))

def _active_main_menu():
        main_menu = WP_MAIN_MENU_NAME
        if main_menu:
            return "wp menu location assign '%s' primary" % main_menu
        else:
            return ""

def _make_clean_strings(old_url, new_url):
    replace_string = "wp search-replace '%s' '%s' " % (old_url, new_url)
    clean_list = ['wp cache flush', replace_string, "wp db repair"]
    return clean_list


def clean_import(old_url, new_url, location="local"):
    for wpcommand in _make_clean_strings(old_url, new_url):
        if location == "local":
            local(wpcommand)
        else:
            run(wpcommand)

def clean_local_import():
    local("wp cache flush")
    local("wp search-replace '%s' '%s' " % (WPCONFIG['remote']['url'],WPCONFIG['local']['url']))
    local("wp db repair")

def clean_hosting_import():
    local("wp cache flush")
    local("wp search-replace '%s' '%s' " % (WPCONFIG['remote']['url'],WPCONFIG['hosting']['url']))
    local("wp search-replace '%s' '%s' " % (WPCONFIG['local']['url'],WPCONFIG['hosting']['url']))
    local("wp db repair")
    local('rm wp-config.php')

    local('wp core config  --dbname=%s --dbuser=%s --dbpass=%s --dbhost=%s --skip-check --extra-php %s' 
        % ( DBASE['hosting']['name'],
            DBASE['hosting']['user'],
            DBASE['hosting']['password'],
            DBASE['hosting']['host'],
            EXTRA_SECURE_WPCONFIG))
    local("touch .htaccess")
    htaccess_file = "%s/.htaccess" % getHostingDirName()
    with open(htaccess_file, "a") as myfile:
        myfile.write(EXTRA_HTACCESS_SEC)


def clean_remote_update():
    run("wp cache flush")
    run("wp search-replace '%s' '%s' " % (WPCONFIG['local']['url'],WPCONFIG['remote']['url']))
    run("wp db repair")
    run("touch .htaccess")
    #echo 'task goes here' | cat - todo.txt > temp && mv temp todo.txt
    run("echo '%s' .htaccess > temp && mv temp .htaccess" % EXTRA_HTACCESS_SEC)

    # apache_permission, file_permission, dir_permission, git_permission = _make_permission_strings("remote")
    # sudo(apache_permission)
    # sudo(file_permission)
    # sudo(dir_permission)
    # sudo(git_permission)

def adjust_permitions(location):
    if location == "remote":
        with cd(CODE_DIR_REMOTE):
            script_file = open('fix-wordpress-permissions_server.sh')
            sudo(script_file.read())
            script_file.close()
    elif location == "local":
        script_file = open('fix-wordpress-permissions_server.sh')
        script_file.read()
        script_file.close()



def _make_permission_strings(location):
    wp_dir = CODE_DIR_LOCAL
    if location == "remote":
        wp_dir = CODE_DIR_REMOTE
    return [ "chown www-data:www-data -R %s" % wp_dir,
    "find %s -type d -exec chmod 755 {} \;" % wp_dir, 
    "find %s -type f -exec chmod 644 {} \;" % wp_dir,
    "chmod 777 -R %s/.git/" % wp_dir,
   ]


def update_wp_db_site_option_remote():
    run(" wp db import export/db_options.sql")

#SOSPESA
def update_wp_site_option_remote():
    local_wp_url = WPCONFIG["local"]['url']
    remote_wp_url = WPCONFIG['remote']['url']
    local_wp_admin_email = WPCONFIG["local"]['admin_email']
    remote_wp_admin_email = WPCONFIG["remote"]['admin_email']
    updates = _make_update_wp_site_option_list(location = "remote")
    with cd(CODE_DIR_REMOTE):
        for i in updates:
            value = i['option_value']
            key = i['option_name']
            if key =="gmt_offset":
                pass
            else:
                if isinstance(value, basestring):
                    
                    value = value.replace(local_wp_url, remote_wp_url)
                    value = value.replace(local_wp_admin_email,remote_wp_admin_email)
                    value = value.replace(u"\u2018", "'").replace(u"\u2019", "'")
                    if value.startswith("a:15:{"):
                        run("wp option update %s '%s' --format=json" %(key, value))
                    else:
                        run('wp option update %s "%s"' %(key, value))                        
                else:
                    run("wp option update %s %s" % (key, value))


def update_wp_content_remote():
    run(_make_update_wp_content_string(CODE_DIR_REMOTE))

def update_wp_content_local():
    local(_make_update_wp_content_string(CODE_DIR_LOCAL))

def _make_update_wp_content_string(code_dir):
    newest = max(glob.iglob('%s/export/*.[Xx][Mm][Ll]'  % CODE_DIR_LOCAL), key=os.path.getctime)
    news_name = os.path.basename(newest)
    return "wp import %s/export/%s --authors=skip --skip='attachment, image_resize'" % (code_dir, news_name)

def _trasform_json_in_wpcli_value(value):
    if isinstance(value, basestring):
        value = value.replace("True", "1")
        value = value.replace("False", "0")
        return "'%s'" % value
    else:
        if value  == True:
            value = 1
        elif value == False:
            value = 0
        return value 


def update_wp_theme_option_remote():
    url = '%s/export/thememod.json' % WPCONFIG['remote']['url']
    updates = _make_update_wp_theme_option_list(url = url)
    make_wpcli_theme_updates(updates, location="remote", invert=True)


def update_wp_theme_option_local():
    url = '%s/export/thememod.json' % WPCONFIG['local']['url']
    updates = _make_update_wp_theme_option_list(url = url)
    make_wpcli_theme_updates(updates, location="local", invert=False)
    # for i in updtes:
    #     value = i['value']
    #     key = i['key']
    #     value = _trasform_json_in_wpcli_value(value)
    #     local("wp theme mod set %s %s" % (key, value))

        # if isinstance(value, basestring):
        #     value = value.replace("True", "1")
        #     value = value.replace("False", "0")
        #     local("wp theme mod set %s '%s'" % (key, value))
        # else:
        #     if value  == True:
        #         value = 1
        #     elif value == False:
        #         value = 0
        #     local("wp theme mod set %s %s" % (key, value))
        

def make_wpcli_theme_updates(updates, location="remote", invert=True):
    if updates:
        for i in updates:
            value = i['value']
            key = i['key']
            value = _trasform_json_in_wpcli_value(value)
            if isinstance(value, basestring):
                if invert:
                    if location == "remote ":
                        value = value.replace(WPCONFIG['local']['url'], WPCONFIG['remote']['url'])
                    elif location == "hosting":
                        value = value.replace(WPCONFIG['local']['url'], WPCONFIG['hosting']['url'])
                    else:
                        value = value.replace(WPCONFIG['remote']['url'], WPCONFIG['local']['url'])
            if location == "remote":
                with cd(CODE_DIR_REMOTE):
                    run("wp theme mod set %s %s" % (key, value))
            else:
                with lcd(CODE_DIR_LOCAL):
                    local("wp theme mod set %s %s" % (key, value))


def _make_update_wp_theme_option_list(url):
    response = urlopen(url)
    json_obj = json.load(response)
    return json_obj

def _make_update_wp_site_option_list(location):
    wp_config = WPCONFIG[location]
    url = '%s/export/settings.json' % wp_config['url']
    response = urlopen(url)
    json_obj = json.load(response)
    return json_obj



def istall_wp_extra_remote():
    with cd(CODE_DIR_REMOTE):
        sudo("/usr/local/bin/composer self-update")
        run("composer install")

def istall_wp_extra_local():
    with lcd(CODE_DIR_LOCAL):
        local("composer install")

def install_wp_core_remote():
    with cd(CODE_DIR_REMOTE):
        with settings(warn_only=True):
            run("wp core download --locale=it_IT")
            run(_make_config_wp_string(location="remote"))
        run(_make_install_wp_string(location="remote"))
        

def install_wp_core_local():
    with lcd(CODE_DIR_LOCAL):
        with settings(warn_only=True):
            local("wp core download --locale=it_IT")
            local(_make_config_wp_string(location="local"))
        local(_make_install_wp_string(location="local"))

        


def update_wp_core_remote():
    with cd(CODE_DIR_REMOTE):
        run("wp core update")
        run("wp core update-db")

def update_wp_core_local():
    with lcd(CODE_DIR_LOCAL):
        local("wp core update ")
        local("wp core update-db")

def update_wp_core_hosting():
    hostingTempDir = getHostingDirName()
    with lcd(hostingTempDir):
        local("wp core update ")
        local("wp core update-db")

def update_wp_extra_remote():
    with cd(CODE_DIR_REMOTE):
        sudo("/usr/local/bin/composer self-update")
        run("composer update")

def update_wp_extra_local():
    with lcd(CODE_DIR_LOCAL):
        local("composer update")

def update_wp_extra_hosting():
    hostingTempDir = getHostingDirName()
    with lcd(hostingTempDir):
        local("composer update")

def _get_latest_source(source_folder):
    if exists(source_folder + '/.git'):  #1
        run('cd %s && git fetch' % (source_folder,))  #23
    else:
        run('git clone --depth=1 %s %s' % (REPO_URL, source_folder))  #4
    current_commit = local("git log -n 1 --format=%H", capture=True)  #5
    run('cd %s && git reset --hard %s' % (source_folder, current_commit))  #6

def _make_create_db_string(dbname, dbuser, dbpassword ):
    return 'echo "CREATE DATABASE IF NOT EXISTS %s;" | mysql --user=%s --password=%s' % (dbname, dbuser, dbpassword)

def getHostingDirName():
    return "%s/hosting" % CODE_DIR_LOCAL

def getTempDbName():
    return  "%s_temp" % DBASE['local']['name']

def create_db(location="local"):
    db_config = DBASE.get(location)
    if location == "local":
        local(_make_create_db_string(db_config['name'], db_config['user'], db_config['password']))
    elif location == "hosting":
        local(_make_create_db_string(getTempDbName(), DBASE['local']['user'], DBASE['local']['password']))
    else:
        run(_make_create_db_string(db_config['name'], db_config['user'], db_config['password']))

def export_last_content():
    with cd(CODE_DIR_REMOTE):
        run("%s --dir=%s/export/server/" % (_make_export_string(location='remote'), CODE_DIR_REMOTE))

def export_last_content():
    with cd(CODE_DIR_REMOTE):
        run("%s --dir=%s/export/server/" % (_make_export_string(location='remote'), CODE_DIR_REMOTE))

def export_wp_content(location = "remote", many_files=False):
    #da fare meglio 

    if location == "remote":
        with cd(CODE_DIR_REMOTE):
            run("%s --dir=%s/export/server/" % (_make_export_string(location='remote'), CODE_DIR_REMOTE))
            #tree.find("%s" % WPCONFIG['local']['url']).text = "%s" % WPCONFIG['remote']['url']
            run("wp theme mod get --all --format=json > export/server/thememod.json")
            run("wp option list --format=json > export/server/settings.json")
            run("wp db export --tables=wp_options,wp_posts,wp_postmeta --add-drop-table export/server/db_options_posts.sql")
            run("wp plugin list --status=active --format=json > export/server/plugin_list.json")
    else:
        with lcd(CODE_DIR_LOCAL): 
            local("%s --dir=%s/export/" % (_make_export_string(location='local'), CODE_DIR_LOCAL))
            newest = max(glob.iglob('%s/export/*.[Xx][Mm][Ll]'  % CODE_DIR_LOCAL), key=os.path.getctime)
            with open(newest, 'rb+') as f:
                tree = ET.parse(f)
                root = tree.getroot()
                for elem in root.getiterator():
                    if elem.text:
                        elem.text = elem.text.replace("%s" % WPCONFIG['local']['url'], "%s" % WPCONFIG['remote']['url'])
                    if elem.tail:
                        elem.tail = elem.tail.replace("%s" % WPCONFIG['local']['url'], "%s" % WPCONFIG['remote']['url'])

                f.seek(0)
                f.write(ET.tostring(tree, encoding='UTF-8', xml_declaration=True))
                f.truncate()
            #tree.find("%s" % WPCONFIG['local']['url']).text = "%s" % WPCONFIG['remote']['url']
            local("wp theme mod get --all --format=json > export/thememod.json")
            local("wp option list --format=json > export/settings.json")
            local("wp db export --tables=wp_options,wp_posts,wp_postmeta --add-drop-table export/db_options.sql")
            
            if many_files:
                #splitto i commit perchè troppo pesante  usando  GitPython 
                #http://gitpython.readthedocs.io/en/stable/tutorial.html#tutorial-label
                local('echo "sto separando i files per l upload"')
                repo = git.Repo('.git/')
                filestoadd = repo.untracked_files
                n = 0
                for file in filestoadd:
                    repo.index.add([file])
                    n = n+1 
                    if n % 5 == 0:
                        repo.index.commit('aggiunta files separati')
                        heads = repo.heads
                        master = heads.master
                        try:
                            repo.remotes['origin'].push()
                            print "commit effettuato %s" % master.commit
                        except Exception, e:
                            print e
                            print "errore al commit: %s" % master.commit
                            raise 
                            

            local('git add .')
            local('git commit -am"export contenuti"')
            local('git push %s master' % GIT_REMOTE_NAME)

def _make_export_string(location):
    wp_config = WPCONFIG[location]

    return "wp export --user='%s' " % (wp_config['admin_user'])



def deploy_rebuild(export = False ):
    with settings(warn_only=True):
        with cd(CODE_DIR_REMOTE):    
            run("wp db drop")
            sudo("rm -R %s" % CODE_DIR_REMOTE)
    deploy_start(export)


def deploy(only_pull = False, sync_db = False, export = False, many_files=False):
    if export:
        export_wp_content(location="local", many_files=many_files)
    with cd(CODE_DIR_REMOTE):


        if only_pull:
            _get_latest_source(CODE_DIR_REMOTE)
        else:
            update_wp(location="remote")
        adjust_permitions("remote")



def deploy_hosting():
    #
    # deve installare un nuovo wp importare i dati su un db temporaneo per poi 
    # modificare il contenuto in base ai dati dell hosting
    create_db(location='hosting')

    localHostingDir = getHostingDirName()
    if not os.path.exists(localHostingDir):
        local('sudo mkdir -p %s' % localHostingDir)
        local('sudo chmod 777 %s' % localHostingDir)
        local('git clone --depth=1 %s %s' % (REPO_URL, localHostingDir))  #4  
    else:
        local('cd %s && git fetch' % (localHostingDir,))
  
    with lcd(localHostingDir):
        with settings(warn_only=True):
            local("wp core download --locale=it_IT")
            local('rm wp-config.php')
            local(_make_config_wp_string(location="hosting"))
            local("composer update")
        
        local(_make_install_wp_string(location="hosting"))
        local_import_last(localHostingDir)
        clean_hosting_import()
        print " fine "        



def get_temp_hosting_dbconf():
    tempdbname = getTempDbName()
    db_config = {
        'user': DBASE['local']['user'],
        'password': DBASE['local']['password'],
        'name': tempdbname

    }

    return db_config


def _make_config_wp_string(location='local'):
    db_config = DBASE.get(location)
    if location == "hosting":
        db_config =get_temp_hosting_dbconf()

    wp_config_string = "wp core config --dbname=%s --dbuser=%s --dbpass=%s --extra-php %s" % (
        db_config['name'], 
        db_config['user'], 
        db_config['password'],
        EXTRA_SECURE_WPCONFIG)
  
    return wp_config_string


def _make_install_wp_string(location='local'):
    wp_config = WPCONFIG[location]
    wp_install_string = "wp core install --url=%s --title='%s' --admin_user=%s --admin_password='%s' --admin_email=%s"\
     % (wp_config['url'], SITE_NAME, wp_config['admin_user'], wp_config["admin_pass"],
            wp_config['admin_email'])

    return wp_install_string

def _make_active_theme_string():
    return "wp theme activate %s" % THEME_GIT_NAME

def _make_adminuser_string(location):
    wp_config = WPCONFIG[location]
    return "wp user create %s %s --role=administrator --user_pass=%s" % (wp_config['admin_user'], 
        wp_config['admin_email'], wp_config['admin_pass'])

def rebuild_from_files():
    #
    # aggiusta wp da files locali
    #
    old_site_url = local('wp option get siteurl', capture=True)
    new_site_url =  WPCONFIG['local']['url']
    clean_import(old_site_url, new_site_url)
    local(_make_config_wp_string('local'))
    local(_make_adminuser_string('local'))




def deploy_start(export = False):
    #
    # il primo deploy da eseguire solo la prima volta 
    #
    sudo('mkdir -p %s' % CODE_DIR_REMOTE)
    sudo('chmod 777 %s' % CODE_DIR_REMOTE)
    create_db(location="remote")
    #run('mkdir -p %s' % CODE_DIR_REMOTE)
    with cd(CODE_DIR_REMOTE):

        install_wp(location="remote")
        if export:
            export_wp_content(location="local")
        update_wp(location="remote")
        adjust_permitions("remote")
        run("touch .htaccess")
        sudo("chmod 777 .htaccess")
